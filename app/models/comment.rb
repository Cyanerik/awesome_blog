class Comment < ApplicationRecord
  belongs_to :entry
  has_one :user
end
