class AddEntryIdToEntries < ActiveRecord::Migration[5.1]
  def change
    add_column :entries, :entry_type_id, :integer
  end
end
