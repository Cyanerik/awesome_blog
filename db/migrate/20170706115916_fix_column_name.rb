class FixColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :entry_types, :type, :name
  end
end
