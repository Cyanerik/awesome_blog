class RemovePublishedAtFromEntries < ActiveRecord::Migration[5.1]
  def change
    remove_column :entries, :published_at
  end
end
