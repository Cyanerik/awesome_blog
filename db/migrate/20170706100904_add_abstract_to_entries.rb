class AddAbstractToEntries < ActiveRecord::Migration[5.1]
  def change
    add_column :entries, :abstract, :text
  end
end
